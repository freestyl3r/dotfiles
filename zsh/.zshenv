typeset -U path
path=(
	$path
	/home/free/.gem/ruby/2.1.0/bin
	/home/free/bin
)

export XDG_CONFIG_HOME=~/.config
export GOPATH=/home/free/programming/go
